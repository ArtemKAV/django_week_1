from django.http import HttpResponse


def hellodjango(request):
    return HttpResponse('Hello Django')


def handler_name(request, name):
    return HttpResponse(f'Hello {name}')


def handler_date(request):
    return HttpResponse('22.03.2022')


def handler_date1(request, date):
    try:
        if date == 'year':
            context = '2022'
        elif date == 'month':
            context = '03'
        elif date == 'day':
            context = '22'
        return HttpResponse(context)

    except:
        return HttpResponse(f'Not found {date}')
