from django.urls import path
from .views import *

app_name = 'firstapp'

urlpatterns = [
    path('', hellodjango),
    path('date/', handler_date),
    path('date/<str:date>', handler_date1),
    path('<str:name>/', handler_name),

]
