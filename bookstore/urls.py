from django.urls import path
from .views import *

app_name = 'bookstore'

urlpatterns = [
    path('books/', book_list, name='book-list'),
    path('books/<int:pk>/', book_detail, name='book-detail'),
    path('authors/', author_list, name='author-list'),
    path('authors/<int:pk>', author_detail, name='author-detail'),
    path('authors/<int:pk>/books', books_by_author, name='books-by-author')
]
