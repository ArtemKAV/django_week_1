from django.http import HttpResponse
from django.shortcuts import render

books = [
    {
        'id': 1,
        'title': 'Python Crash Course',
        'released_year': 2016,
        'description': 'It does what it says on the tin, and it does it really well. The book starts out with a '
                       'walkthrough of the basic Python elements and data structures, working through variables, '
                       'strings, numbers, lists, and tuples, outlining how you work with each of them. Next, '
                       'if statements and logical tests are covered, followed by a dive into dictionaries. After '
                       'that, the book covers user input, while loops, functions, classes, and file handling, '
                       'as well as code testing and debugging. That’s just the first half of the book! In the second '
                       'half, you work on three major projects, creating some clever, fun applications. The first '
                       'project is an Alien Invasion game, essentially Space Invaders, developed using the pygame '
                       'package. You design a ship (using classes), then program how to pilot it and make it fire '
                       'bullets. Then, you design several classes of aliens, make the alien fleet move, and make it '
                       'possible to shoot them down. Finally, you add a scoreboard and a list of high scores to '
                       'complete the game. After that, the next project covers data visualization with matplotlib, '
                       'random walks, rolling dice, and a little bit of statistical analysis, creating graphs and '
                       'charts with the pygal package. You learn how to download data in a variety of formats, '
                       'import it into Python, and visualize the results, as well as how to interact with web APIs, '
                       'retrieving and visualizing data from GitHub and HackerNews. The third project walks you '
                       'through the creation of a complete web application using Django to set up a Learning Log to '
                       'track what users have been studying. It covers how to install Django, set up a project, '
                       'design your models, create an admin interface, set up user accounts, manage access controls '
                       'on a per-user basis, style your entire app with Bootstrap, and then finally deploy it to '
                       'Heroku. This book is well written and nicely organized. It presents a large number of useful '
                       'exercises as well as three challenging and entertaining projects that make up the second half '
                       'of the book. (Reviewed by David Schlesinger.)',
        'author_id': 1,
    },
    {
        'id': 2,
        'title': 'Head-First Python, 2nd edition',
        'released_year': 2016,
        'description': 'I really like the Head-First series of books, although they’re admittedly lighter weight in '
                       'overall content than many of the other recommendations in this section. The trade-off is the '
                       'that this approach makes the book more user-friendly. If you’re the kind of person who likes '
                       'to learn things one small, fairly self-contained chunk at a time, and you want to have lots '
                       'of concrete examples and illustrations of the concepts involved, then the Head-First series '
                       'is for you. The publisher’s website has the following to say about their approach: “Based on '
                       'the latest research in cognitive science and learning theory, Head-First Python uses a '
                       'visually rich format to engage your mind, rather than a text-heavy approach that puts you to '
                       'sleep. Why waste your time struggling with new concepts? This multi-sensory learning '
                       'experience is designed for the way your brain really works.” (Source) Chock full of '
                       'illustrations, examples, asides, and other tidbits, Head-First Python is consistently '
                       'engaging and easy to read. This book starts its tour of Python by diving into lists and '
                       'explaining how to use and manipulate them. It then goes into modules, errors, '
                       'and file handling. Each topic is organized around a unifying project: building a dynamic '
                       'website for a school athletic coach using Python through a Common Gateway Interface (CGI). '
                       'After that, the book spends time teaching you how to use an Android application to interact '
                       'with the website you created. You learn to handle user input, wrangle data, and look into '
                       'what’s involved in deploying and scaling a Python application on the web. While this book '
                       'isn’t as comprehensive as some of the others, it covers a good range of Python tasks in a way '
                       'that’s arguably more accessible, painless, and effective. This is especially true if you find '
                       'the subject of writing programs somewhat intimidating at first. This book is designed to '
                       'guide you through any challenge. While the content is more focused, this book has plenty of '
                       'material to keep you busy and learning. You will not be bored. If you find most programming '
                       'books to be too dry, this could be an excellent book for you to get started in Python. ('
                       'Reviewed by David Schlesinger and Steven C. Howell.)',
        'author_id': 2,
    },
    {
        'id': 3,
        'title': 'Invent Your Own Computer Games with Python, 4th edition',
        'released_year': 2017,
        'description': 'If games are your thing, or you even have a game idea of your own, this would be the perfect '
                       'book to learn Python. In this book, you learn the fundamentals of programming and Python with '
                       'the application exercises focused on building classic games. Starting with an introduction to '
                       'the Python shell and the REPL loop, followed by a basic “Hello, World!” script, '
                       'you dive right into making a basic number-guessing game, covering random numbers, '
                       'flow control, type conversion, and Boolean data. After that, a small joke-telling script is '
                       'written to illustrate the use of print statements, escape characters, and basic string '
                       'operations. The next project is a text-based cave exploration game, Dragon’s Realm, '
                       'which introduces you to flowcharts and functions, guides you through how to define your own '
                       'arguments and parameters, and explains Boolean operators, global and local scope, '
                       'and the sleep() function. After a brief detour into how to debug your Python code, '
                       'you next implement the game of Hangman, using ASCII artwork, while learning about lists, '
                       'the in operator, methods, elif statements, the random module, and a handful of string '
                       'methods. You then extend the Hangman game with new features, like word lists and difficulty '
                       'levels, while learning about dictionaries, key-value pairs, and assignment to multiple '
                       'variables. Your next project is a Tic-Tac-Toe game, which introduces some high-level '
                       'artificial intelligence concepts, shows you how to short-circuit evaluation in conditionals, '
                       'and explains the None value as well as some different ways of accessing lists. Your journey '
                       'through the rest of the book proceeds in a similar vein. You’ll learn nested loops while '
                       'building a Mastermind-style number guessing game, Cartesian coordinates for a Sonar Hunt '
                       'game, cryptography to write a Caesar cipher, and artificial intelligence when implementing '
                       'Reversi (also known as Othello), in which the computer can play against itself. After all of '
                       'this, there’s a dive into using graphics for your games with PyGame: you’ll cover how to '
                       'animate the graphics, manage collision detection, as well as use sounds, images, and sprites. '
                       'To bring all these concepts together, the book guides you through making a graphical '
                       'obstacle-dodging game. This book is well done, and the fact that each project is a '
                       'self-contained unit makes it appealing and accessible. If you’re someone who likes to learn '
                       'by doing, then you’ll enjoy this book. The fact that this book introduces concepts only as '
                       'needed can be a possible disadvantage. While it’s organized more as a guide than a reference, '
                       'the broad range of contents taught in the context of familiar games makes this one of the '
                       'best books for learning Python. (Reviewed by David Schlesinger.)',
        'author_id': 3,
    },
    {
        'id': 4,
        'title': 'Think Python: How to Think Like a Computer Scientist, 2nd edition',
        'released_year': 2015,
        'description': 'If learning Python by creating video games is too frivolous for you, consider Allen Downey’s '
                       'book Think Python, which takes a much more serious approach. As the title says, the goal of '
                       'this book is to teach you how coders think about coding, and it does a good job of it. '
                       'Compared to the other books, it’s drier and organized in a more linear way. The book focuses '
                       'on everything you need to know about basic Python programming, in a very straightforward, '
                       'clear, and comprehensive way. Compared to other similar books, it doesn’t go quite as deep '
                       'into some of the more advanced areas, instead covering a wider range of material, '
                       'including topics the other books don’t go anywhere near. Examples of such topics include '
                       'operator overloading, polymorphism, analysis of algorithms, and mutability versus '
                       'immutability. Previous versions were a little light on exercises, but the latest edition has '
                       'largely corrected this shortcoming. The book contains four reasonably deep projects, '
                       'presented as case studies, but overall, it has fewer directed application exercises compared '
                       'to many other books. If you like a step-by-step presentation of just the facts, and you want '
                       'to get a little additional insight into how professional coders look at problems, '
                       'this book is a great choice. (Reviewed by David Schlesinger and Steven C. Howell.)',
        'author_id': 4,
    },
    {
        'id': 7,
        'title': 'Think Python: Book1',
        'released_year': 2015,
        'description': 'If learning Python by creating video games is too frivolous for you, consider Allen Downey’s '
                       'book Think Python, which takes a much more serious approach. As the title says, the goal of '
                       'this book is to teach you how coders think about coding, and it does a good job of it. '
                       'Compared to the other books, it’s drier and organized in a more linear way. The book focuses '
                       'on everything you need to know about basic Python programming, in a very straightforward, '
                       'clear, and comprehensive way. Compared to other similar books, it doesn’t go quite as deep '
                       'into some of the more advanced areas, instead covering a wider range of material, '
                       'including topics the other books don’t go anywhere near. Examples of such topics include '
                       'operator overloading, polymorphism, analysis of algorithms, and mutability versus '
                       'immutability. Previous versions were a little light on exercises, but the latest edition has '
                       'largely corrected this shortcoming. The book contains four reasonably deep projects, '
                       'presented as case studies, but overall, it has fewer directed application exercises compared '
                       'to many other books. If you like a step-by-step presentation of just the facts, and you want '
                       'to get a little additional insight into how professional coders look at problems, '
                       'this book is a great choice. (Reviewed by David Schlesinger and Steven C. Howell.)',
        'author_id': 4,
    },
    {
        'id': 5,
        'title': 'Effective Computation in Physics: Field Guide to Research with Python',
        'released_year': 2015,
        'description': 'This is the book I wish I had when I was first learning Python. Despite its name, this book '
                       'is an excellent choice for people who don’t have experience with physics, research, '
                       'or computational problems. It really is a field guide for using Python. On top of actually '
                       'teaching you Python, it also covers the related topics, like the command-line and version '
                       'control, as well as the testing and deploying of software. In addition to being a great '
                       'learning resource, this book will also serve as an excellent Python reference, as the topics '
                       'are well organized with plenty of interspersed examples and exercises. The book is divided '
                       'into four aptly named sections: Getting Started, Getting it Done, Getting it Right, '
                       'and Getting it Out There. The Getting Started section contains everything you need to hit the '
                       'ground running. It begins with a chapter on the fundamentals of the bash command-line. (Yes, '
                       'you can even install bash for Windows.) The book then proceeds to explain the foundations of '
                       'Python, hitting on all the expected topics: operators, strings, variables, containers, logic, '
                       'and flow control. Additionally, there is an entire chapter dedicated to all the different '
                       'types of functions, and another for classes and object-oriented programming. Building on this '
                       'foundation, the Getting it Done section moves into the more data-centric area of Python. Note '
                       'that this section, which takes up approximately a third of the book, will be most applicable '
                       'to scientists, engineers, and data scientists. If that is you, enjoy. If not, feel free to '
                       'skip ahead, picking out any pertinent sections. But be sure to catch the last chapter of the '
                       'section because it will teach you how to deploy software using pip, conda, virtual machines, '
                       'and Docker containers. For those of you who are interested in working with data, the section '
                       'begins with a quick overview of the essential libraries for data analysis and visualization. '
                       'You then have a separate chapter dedicated to teaching you the topics of regular expressions, '
                       'NumPy, data storage (including performing out-of-core operations), specialized data '
                       'structures (hash tables, data frames, D-trees, and k-d trees), and parallel computation. The '
                       'Getting it Right section teaches you how to avoid and overcome many of the common pitfalls '
                       'associated with working in Python. It begins by extending the discussion on deploying '
                       'software by teaching you how to build software pipelines using make. You then learn how to '
                       'use Git and GitHub to track, store, and organize your code edits over time, a process known '
                       'as version control. The section concludes by teaching you how to debug and test your code, '
                       'two incredibly valuable skills. The final section, Getting it Out There, focuses on '
                       'effectively communicating with the consumers of your code, yourself included. It covers the '
                       'topics of documentation, markup languages (primarily LaTeX), code collaboration, and software '
                       'licenses. The section, and book, concludes with a long list of scientific Python projects '
                       'organized by topic. This book stands out because, in addition to teaching all the '
                       'fundamentals of Python, it also teaches you many of the technologies used by Pythonistas. '
                       'This is truly one of the best books for learning Python. It also serves as a great reference, '
                       'will a full glossary, bibliography, and index. The book definitely has a scientific Python '
                       'spin, but don’t worry if you do not come from a scientific background. There are no '
                       'mathematical equations, and you may even impress your coworkers when they see you are on '
                       'reading up on Computational Physics! (Reviewed by Steven C Howell.)',
        'author_id': 5,
    },
    {
        'id': 6,
        'title': 'Learn Python 3 the Hard Way',
        'released_year': 2016,
        'description': 'Learn Python the Hard Way is a classic. I’m a big fan of the book’s approach. When you learn '
                       '“the hard way,” you have to: Type in all the code yourself Do all the exercises Find your own '
                       'solutions to problems you run into The great thing about this book is how well the content is '
                       'presented. Each chapter is clearly presented. The code examples are all concise, '
                       'well constructed, and to the point. The exercises are instructive, and any problems you run '
                       'into will not be at all insurmountable. Your biggest risk is typographical errors. Make it '
                       'through this book, and you’ll definitely no longer be a beginner at Python. Don’t let the '
                       'title put you off. The “hard way” turns out to be the easy way if you take the long view. '
                       'Nobody loves typing a lot of stuff in, but that’s what programming actually involves, '
                       'so it’s good to get used to it from the start. One nice thing about this book is that it has '
                       'been refined through several editions now, so any rough edges have been made nice and smooth '
                       'by now. The book is constructed as a series of over fifty exercises, each building on the '
                       'previous, and each teaching you some new feature of the language. Starting from Exercise 0, '
                       'getting Python set up on your computer, you begin writing simple programs. You learn about '
                       'variables, data types, functions, logic, loops, lists, debugging, dictionaries, '
                       'object-oriented programming, inheritance, and packaging. You even create a simple game using '
                       'a game engine. The next sections cover concepts like automated testing, lexical scanning on '
                       'user input to parse sentences, and the lpthw.web package, to put your game up on the web. Zed '
                       'is an engaging, patient writer who doesn’t gloss over the details. If you work through this '
                       'book the right way—the “hard way,” by following up on the study suggestions provided '
                       'throughout the text as well as the programming exercises—you’ll be well beyond the beginner '
                       'programmer stage when you’ve finished. (Reviewed by David Schlesinger.)',
        'author_id': 6,
    },
    {
        'id': 8,
        'title': 'Learn Python Book1',
        'released_year': 2011,
        'description': 'Something about book',
        'author_id': 6,
    },
]

authors = [
    {
        'id': 1,
        'first_name': 'Eric',
        'last_name': 'Matthes',
        'age': 50,
    },
    {
        'id': 2,
        'first_name': 'Paul',
        'last_name': 'Barry',
        'age': 48,
    },
    {
        'id': 3,
        'first_name': 'Al',
        'last_name': 'Sweigart',
        'age': 37,
    },
    {
        'id': 4,
        'first_name': 'Allen B.',
        'last_name': 'Downey',
        'age': 51,
    },
    {
        'id': 5,
        'first_name': 'Anthony',
        'last_name': 'Scopatz',
        'age': 45,
    },
    {
        'id': 6,
        'first_name': 'Zed A.',
        'last_name': 'Shaw',
        'age': 39,
    },
]


def book_list(request):
    return render(request, 'bookstore/books_list.html', context={'books': books})


def author_list(request):
    return render(request, 'bookstore/author_list.html', context={'authors': authors})


def book_detail(request, pk):
    try:
        book = books[int(pk) - 1]
        for author in authors:
            if book['author_id'] == author['id']:
                context_author = author
        context = {
            'book': book,
            'author': context_author
        }
        return render(request, 'bookstore/book_detail.html', context=context)
    except:
        return HttpResponse(f'Sorry, book with id: {pk}, not found')


def author_detail(request, pk):
    try:
        for a in authors:
            if a['id'] == pk:
                author = a

        context = {'author': author}
        return render(request, 'bookstore/author_detail.html', context=context)
    except:
        return HttpResponse(f'Sorry, author with id: {pk}, not found')


def books_by_author(request, pk):
    context_books = []
    for book in books:
        if book['author_id'] == int(pk):
            context_books.append(book)
    for a in authors:
        if a['id'] == pk:
            author = a
    context = {
        'books': context_books,
        'author': author,
    }
    return render(request, 'bookstore/author_books.html', context=context)
